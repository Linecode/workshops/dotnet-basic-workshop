using System;
using System.Collections.Generic;

namespace Library.Domain
{
    public class Order
    {
        public DateTime Date { get; set; }
        public List<BookOrdered> BooksOrderedList { get; set; }

        public Order()
        {
            Date = DateTime.Now;
            BooksOrderedList = new List<BookOrdered>();
        }

        public override string ToString()
        {
            var str = $"Order: {Date}";

            foreach (var bookOrdered in BooksOrderedList)
            {
                str += $"\n Book: {bookOrdered.BookId} Count: {bookOrdered.NumberOrdered}";
            }

            return str;
        }
    }
}
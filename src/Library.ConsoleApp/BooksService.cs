using System;
using Library.Domain;
using Library.Persistence;

namespace Library.ConsoleApp
{
    public class BooksService
    {
        private readonly BooksRepository _repository;

        public BooksService(BooksRepository repository)
        {
            _repository = repository;
        }

        public void AddBook()
        {
            Console.Write("Podaj tytul: ");
            var title = Console.ReadLine();
            Console.Write("Podaj autora: ");
            var author = Console.ReadLine();
            Console.Write("Podaj rok wydania: ");
            var year = Convert.ToInt32(Console.ReadLine());
            Console.Write("Podaj ISBN: ");
            var isbn = Console.ReadLine();
            Console.Write("Podaj liczbe: ");
            var count = Convert.ToInt32(Console.ReadLine());
            Console.Write("Podaj cene: ");
            var price = Convert.ToDecimal(Console.ReadLine());

            var newBook = new Book(title, author, year, isbn, count, price);
            _repository.Insert(newBook);
        }

        public void RemoveBook()
        {
            Console.Write("Podaj tytul: ");
            var titleForRemove = Console.ReadLine();
            _repository.RemoveByTitle(titleForRemove);
        }

        public void ListBooks()
        {
            foreach (var book in _repository.GetAll())
            {
                Console.WriteLine(book.ToString());
            }
        }

        public void ChangeState()
        {
            Console.Write("Podaj tytul: ");
            var title = Console.ReadLine();
            Console.Write("Podaj zmiane stanu: ");
            var stateChange = Convert.ToInt32(Console.ReadLine());
            _repository.ChangeState(title, stateChange);
        }
    }
}
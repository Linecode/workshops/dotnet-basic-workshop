﻿using System;
using Library.Persistence;

namespace Library.ConsoleApp
{
    class Program
    {
        private static BooksService _booksService;
        private static OrderService _orderService;
        
        static void Main(string[] args)
        {
            if (!Authorize()) return;
            
            var repository = new BooksRepository();
            _booksService = new BooksService(repository);
            var orderRepository = new OrderRepository();
            _orderService = new OrderService(orderRepository);
            var command = "";

            while (!command.Equals("wyjdz"))
            {
                DisplayMenu();
                command = Console.ReadLine();
                HandleCommand(command);
            }
        }

        private static bool Authorize()
        {
            Console.Write("Podaj nazwe uzytkownika: ");
            var userName = Console.ReadLine();
            Console.Write("Podaj haslo: ");
            var password = Console.ReadLine();
            
            if (userName is null)
                throw new Exception("UserName not provided");
            
            if (password is null)
                throw new Exception("Password not provided");

            return userName.Equals("Admin") && password.Equals("password");
        }

        private static void HandleCommand(string command)
        {
            switch (command)
            {
                case "dodaj":
                    Console.Clear();
                    _booksService.AddBook();
                    AfterCommand();
                    break;
                    
                case "wypisz":
                    Console.Clear();
                    _booksService.ListBooks();
                    AfterCommand();
                    break;
                        
                case "zmien":
                    Console.Clear();
                    _booksService.ChangeState();
                    AfterCommand();
                    break;

                case "usun":
                    Console.Clear();
                    _booksService.RemoveBook();
                    AfterCommand();
                    break;
                        
                case "dodaj zamowienie":
                    Console.Clear();
                    _orderService.PlaceOrder();
                    AfterCommand();
                    break;
                        
                case "lista zamowien":
                    Console.Clear();
                    _orderService.ListAll();
                    AfterCommand();
                    break;
                    
                case "wyjdz":
                    break;
                    
                default:
                    throw new Exception("Unsupported command");
            }
        }

        private static void AfterCommand()
        {
            Console.WriteLine("Nacisnij dowolny przycisk aby kontynouwac.");
            Console.ReadKey();
        }

        private static void DisplayMenu()
        {
            Console.Clear();
                
            Console.WriteLine("Witaj w panelu administracyjnym!");
            Console.WriteLine("Co chcesz teraz zrobić? ");
            Console.WriteLine("dodaj - dodaj ksiazke");
            Console.WriteLine("wypisz - wypisz obecny stan ksiazek");
            Console.WriteLine("zmien - zmien stan magazynowy dla wybranego tytulu");
            Console.WriteLine("usun - usun ksiazke");
            Console.WriteLine("dodaj zamowienie - formularz dodawania zamowienia");
            Console.WriteLine("lista zamowien - pokazuje wszystkie zamowienia");
            Console.WriteLine("wyjdz - usun ksiazke");
        }
    }
}

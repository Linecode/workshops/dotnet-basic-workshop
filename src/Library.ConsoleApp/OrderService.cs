using System;
using Library.Domain;
using Library.Persistence;

namespace Library.ConsoleApp
{
    public class OrderService
    {
        private readonly OrderRepository _orderRepository;

        public OrderService(OrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void PlaceOrder()
        {
            var order = new Order();

            var command = "";

            while (!command.Equals("end"))
            {
                Console.Clear();
                Console.WriteLine("add - dodaj pozycje do zamowienia");
                Console.WriteLine("end - zamknij zamowienie");
                
                command = Console.ReadLine();

                if (command.Equals("add"))
                {
                    Console.Clear();
                    Console.Write("Podaj id ksiazki: ");
                    var id = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Podaj ilosc: ");
                    var number = Convert.ToInt32(Console.ReadLine());
                    
                    order.BooksOrderedList.Add(new BookOrdered
                    {
                        BookId = id,
                        NumberOrdered = number
                    });
                }
            }
            
            _orderRepository.Insert(order);
        }

        public void ListAll()
        {
            foreach (var order in _orderRepository.GetAll())
            {
                Console.WriteLine(order);
            }
        }
    }
}